## WLED Messenger

I had a few days to throw together a Stranger Things prop that would host a webserver and allow you to interact with WLED to light up lightbulbs like the scene where Will is communicating with his mom in the upside down.

In short, this project runs on a web server on a Raspberry Pi and communicates with WLED via the WLED API to convert a message into a sequence of specific LED's lighting up. In my specific case, the Pi is hosting a wireless access point with a captive portal so that guests can connecto the wireless network, and then be brought to a page which allows them to interact with the prop.

This is quickly and crappily thrown together, but there are still some neat things that are done, and I thought I might as well throw it into a Git Repo.

## Hardware

* Raspberry Pi 4 w/ USB WiFi NIC
* DigUno w/ Ethernet
* WS2811 strand

## Django

This is the repo for the django aspect of this project. I have the raspberry pi set up so that systemd runs the app via gunicorn. It is also running nginx, which is proxy_pass'ing the gunicorn socket to port 80

## Hostapd

The second USB WiFi NIC is set up to connect to my home wifi so I can easily work from my laptop via SSH, pull in new software, etc.

The built in wifi NIC is used by hostapd to provide a wifi hotspot that people can connect to. I largely used [this](https://github.com/AdamG-Studio/Captive-Portal) repo as a guide for setting all of that up.

Android reaches out to some well known URLs to determine if the internet is working when connecting to a new access point. There's an iptables rule which shoots all traffic destined for port 80 over to our nginx server, which serves a 302 and tells Android that it's behind a captive portal. This causes Android to launch a browser, which serves the 302'd destination (this web server)

## Networking

I have connected the Ethernet of the DigUno to the Ethernet of the Pi and put them both on a 192.168.100.0/24 network with .100 being the Pi and .101 being WLED.

Hostapd serves up the 192.168.99.0/24 network with the pi being 192.168.99.1 and DHCP leases being given.

A phone connected to hostapd and is given IP 192.168.99.10. It connects to this Django project on 192.168.99.1:80 and the user can type in a message, then click the Say It button. There is some python in frontend/messenger.py, which takes the message, then creates a sequence of WLED API calls from the pi at 192.168.100.100 to WLED at 192.168.100.101 with a short pause between them so the letters appear with time to read them.
