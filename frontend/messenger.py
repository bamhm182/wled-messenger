#!/usr/bin/env python3

import requests
import time

from random import randrange

LED_MAP = {
        'a': 50, 'b': 49, 'c': 48, 'd': 47, 'e': 46,
        'f': 45, 'g': 44, 'h': 43, 'i': 42, 'j': 41,
        'k': 21, 'l': 22, 'm': 23, 'n': 24, 'o': 25,
        'p': 26, 'q': 27, 'r': 28, 's': 29, 't': 30,
        'u': 10, 'v':  9, 'w':  8, 'x':  7, 'y':  6,
        'z':  5
}


class WLEDMessenger:
    def __init__(self, wled_ip, wled_port):
        self.wled_ip = wled_ip
        self.wled_port = wled_port
        self.wled_api = f'http://{wled_ip}:{wled_port}/win&'

    def clear_lights(self):
        command = f'FX=0&S=0&S2=50&R={randrange(255)}&G={randrange(255)}&B={randrange(255)}&SB=255'
        requests.get(self.wled_api + command)
        time.sleep(1)
        command = 'FX=0&S=0&S2=50&SB=0'
        requests.get(self.wled_api + command)
        time.sleep(1)

    def crazy_lights(self):
        command = f'S=0&S2=50&SB=255&FX=74&SX=255&IX=128'
        requests.get(self.wled_api + command)
        time.sleep(10)

    def write_message(self, message):
        message = ''.join([l for l in message if l.isalpha()]).lower()
        self.clear_lights()
        for letter in message:
            light = LED_MAP.get(letter, 0) - 1
            if light >= 0:
                command = f'FX=0&S={light}&S2={light+1}&R={randrange(255)}&G={randrange(255)}&B={randrange(255)}&SB=255'
                time.sleep(0.75)
                requests.get(self.wled_api + command)
                time.sleep(0.75)
                command = f'FX=0&S={light}&S2={light+1}&SB=0'
                requests.get(self.wled_api + command)
            else:
                time.sleep(0.75)

        time.sleep(1)

        if message == 'run':
            self.crazy_lights()
        self.clear_lights()


if __name__ == '__main__':
    wm = WLEDMessenger('192.168.100.101', '80')
    while True:
        wm.write_message('hello world')
