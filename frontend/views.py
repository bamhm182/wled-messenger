from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView
from .forms import SpeakForm, EightBallForm
from .messenger import WLEDMessenger
import random


def homePageView(request):
    if request.method == 'POST':
        form = SpeakForm(request.POST)
        print(request.POST)
        if form.is_valid():
            message = form.cleaned_data.get('message', 'i am busted')
            if message == '':
                message = 'test'
        else:
            message = random.choice(['yes', 'no', 'ask again', 'run', 'not a chance'])
        wm = WLEDMessenger('192.168.100.101', '80')
        wm.write_message(message)
        return HttpResponseRedirect('/')
    speak_form = SpeakForm()
    eight_ball_form = EightBallForm()
    return render(request, 'home.html', {'speak_form': speak_form, 'eight_ball_form': eight_ball_form})
