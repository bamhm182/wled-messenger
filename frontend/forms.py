from django import forms

class SpeakForm(forms.Form):
    message = forms.CharField(label='',max_length=20)

class EightBallForm(forms.Form):
    message = forms.CharField(max_length=20, widget=forms.HiddenInput())
